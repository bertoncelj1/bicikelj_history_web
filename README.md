website showing history data of bicikelj stations.

This is a static webpage that connects to bicikelj's API and show its data.


It can served with nginx inside a Docker container with command:

``` $bash
sudo docker run --name bicikelj_web --restart always -p 80:80 -d -v ~/bicikelj_history_web:/usr/share/nginx/html nginx

```
