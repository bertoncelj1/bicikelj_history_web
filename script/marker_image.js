let canvas;
let context;

function initCanvas() {
  if(!canvas) {
   canvas = document.createElement('canvas');
   canvas.width = 100;
   canvas.height = 100;
   context = canvas.getContext('2d');
  }
}

function getImageUrl(color) {
  initCanvas();

  let x = canvas.width/2;
  let y = canvas.height/2;

  context.beginPath();
  context.arc(x,y,canvas.width/2,0,2*Math.PI);
  context.fillStyle = "gray";
  context.fill();

  context.beginPath();
  context.arc(x,y,canvas.width/2*0.95,0,2*Math.PI);
  context.fillStyle = getColor(color);
  context.fill();

  // save canvas image as data url (png format by default)
  return canvas.toDataURL();
}

function getMarkerIcon(color, size) {
  return L.icon({
   iconUrl: getImageUrl(color),
   iconSize:     [size, size],
   iconAnchor:   [size/2, size/2],
   popupAnchor:  [size*4/3, size/3]
  });
}

function getColor(h, s=1, v=0.5) {
  return `hsl(${h*128}, ${s*100}%, ${v*100}%)`
}
