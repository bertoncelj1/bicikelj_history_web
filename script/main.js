let api;
let chart;
let timeFormat = "DD MMM YYYY HH:mm:ss"; // graph's time format
let map;

let deviceSelected = "";
let measurementSelected = "";

$(document).ready(async function() {
  console.log("App is running!")
  spinnerShow(true);
  await initMap();

  api = new BicikeljAPI("http://192.168.1.22:8082");

  let stations = await api.getLatestData();
  console.log(stations[0])
  //printStations(stations);
  addStationMarkers(stations);
  spinnerShow(false);
});


async function initMap() {
  map = L.map('mapid', {
    center: [46.0478, 14.5018],
    zoom: 13
  });

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '<a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
    id: 'mapbox.streets'
  }).addTo(map);


}

function addStationMarkers(stations){
  stations.forEach(station => {
    let ratio = station.free_bikes / station.extra.slots;
    let size = Math.pow(station.extra.slots, 0.4)*7;
    console.log(size, station.extra.slots)

    L.marker([station.latitude, station.longitude],
    {
      icon: getMarkerIcon(ratio, size)
    })
      .addTo(map)
      .on('click', (e) => showStationInfo(station));
  })


}

function showStationInfo(station) {
  console.log("Showing station", station)
  updateTime = moment(station.extra.last_update, "x");
  lastUpdateDuration = moment.duration(moment().diff(updateTime)).humanize();
  $("#stationInfo").html($(`
    <h3>${station.name}</h3>
    <p>Status: ${station.free_bikes}/${station.extra.slots}<p>
    <p>Last Update: ${lastUpdateDuration} ago</p>
  `));
}


function printStations(stations) {
  let stationsEL = $('#stations')
  stationsEL.find('li').remove();
  stations.forEach(station => {
    let txt = `${station.name} ${station.slots}`
    stationsEL.append($('<li/>', {text: txt}))
  })
}

function clearError() {
  $('#error-msg').hide();
}

function showError(msg) {
  $('#error-msg').show()
  $('#error-msg').text(msg)
}

function spinnerShow(show) {
    console.log("sipnner show", show)
    const spinner = $('#spinner');
    if (show) {
        spinner.show();
    } else {
        spinner.hide();
    }
}
