


class BicikeljAPI{
  constructor(url) {
    this.url = url;
  }

  // get latest data directly from the server
  async getLatestData() {
    let data = await requestJson("https://api.citybik.es/v2/networks/bicikelj", "GET", null, false);
    return data.network.stations;
  }

  checkConnection() {
    console.log("checking connection");
    return this.get("")
  }

  getStations() {
    return this.get("/stations");
  }

  getMeasurementsInterval(stationId, start, end) {
    return this.get(`/stations/${enc(stationId)}?start=${enc(start)}&end=${enc(end)}`)
  }

  get(path) {
    return requestJson(this.url + path, "GET");
  }

  post(path) {
    return requestJson(this.url+path, "POST", params);
  }
}

function enc(str) {
  return encodeURIComponent(str)
}

function parseDefaultData() {

}


function requestJson(url, action, params, checkSuccess=true) {
    return new Promise((resolve, reject) => {
          console.log("Request:", url, action, params)
          const xhr = new XMLHttpRequest();
          xhr.overrideMimeType('application/json');
          xhr.addEventListener('load', () => resolve(xhr.responseText));
          xhr.addEventListener('error', () => reject('Failed to connect'));
          xhr.open(action, url);
          if (params) {
              xhr.send(JSON.stringify(params));
          } else {
              xhr.send();
          }
    }).then(responseText => {
        try {
            //console.log("Got data:", JSON.parse(responseText))
            let parsedResponse = JSON.parse(responseText);

            if(checkSuccess) {
              if(parsedResponse.success) {
                return parsedResponse.data;
              } else {
                return Promise.reject('Unsuccessful: ' + parsedResponse)
              }
            }else {
              return JSON.parse(responseText);
            }
        }
        catch (e) {
            return Promise.reject('Invalid response');
        }
    }).catch(err => {
      console.error(err)
      return Promise.reject('Unsuccessful: ' + err)
    });
}
